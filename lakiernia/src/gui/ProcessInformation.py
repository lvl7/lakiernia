from typing import TYPE_CHECKING

from kivy.clock import Clock
from kivy.uix.label import Label
from kivy.uix.popup import Popup

if TYPE_CHECKING:
    from kivy.uix.boxlayout import BoxLayout
    from kivy.uix.gridlayout import GridLayout


class ProcessInformation(Popup):
    def __init__(self, process, machines_line, **kwargs):
        Popup.__init__(self, **kwargs)
        self.process = process
        self.machines_line = machines_line

        self.jobScheduleTable = None

        Clock.schedule_once(self._init_ui, 0)

    def _init_ui(self, dt=0):
        def fill_labels():
            jobNo = 0
            machines_line = self.machines_line
            first_machine = machines_line[list(machines_line.keys())[0]]
            for job in first_machine:
                if job.process_id != self.process.process_id:
                    jobNo += 1
                else:
                    break

            processNo_label = self.ids['processNo_label']
            processNo_label.text = processNo_label.text.format(jobNo + 1)

            prev_processId_label = self.ids['prev_processId_label']
            prev_processId_label.text = prev_processId_label.text.format(
                first_machine[jobNo - 1].process_id if jobNo > 0 else "---"
            )

            next_processId_label = self.ids['next_processId_label']
            next_processId_label.text = next_processId_label.text.format(
                first_machine[jobNo + 1].process_id if jobNo < len(first_machine) - 1 else "---"
            )

            return jobNo

        jobNo = fill_labels()

        self.jobScheduleTable = self.ids['jobScheduleTable']
        jobScheduleTable = self.jobScheduleTable  # type: GridLayout

        row_height = 30
        for name, processes_line in self.machines_line.items():
            process = processes_line[jobNo]
            jobScheduleTable.add_widget(Label(
                text=name,
                size_hint=(None,None),
                height="{}dp".format(row_height)
            ))
            jobScheduleTable.add_widget(Label(
                text="{:>.3f}".format(process.start_time),
                size_hint=(None,None),
                height="{}dp".format(row_height)
            ))
            jobScheduleTable.add_widget(Label(
                text="{:>.3f}".format(process.duration),
                size_hint=(None,None),
                height="{}dp".format(row_height)
            ))
            jobScheduleTable.add_widget(Label(
                text="{:>.3f}".format(process.start_time + process.duration),
                size_hint=(None,None),
                height="{}dp".format(row_height)
            ))

    def open(self, *largs):
        self.title = "Process[{}] information".format(self.process.process_id)
        Popup.open(self, *largs)
