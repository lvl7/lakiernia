import importlib.util
import os

from kivy.lang import Builder

for root, dirs, files in os.walk(__file__.rsplit(os.sep, 1)[0]):
    for file in files:
        if file[0] != '_':
            file_path = os.sep.join([root, file])
            if file[-3:] == '.py':
                module = file[:-3]
                spec = importlib.util.spec_from_file_location(module, os.path.join(root, file))
                spec.loader.exec_module(importlib.util.module_from_spec(spec))
            elif file[-3:] == ".kv":
                Builder.load_file(file_path)
