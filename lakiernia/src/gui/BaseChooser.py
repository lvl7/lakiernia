import os

from kivy.app import App

DIR_PATH = os.path.dirname(__file__)

from tkinter import Tk
from tkinter.filedialog import askopenfilename

from kivy.properties import StringProperty, Clock
from kivy.uix.boxlayout import BoxLayout

DATABASE_EXTENSION = "*.csv"
DEFAULT_DATABASE = os.path.normpath(os.path.join(DIR_PATH, "../../database/10_4m.csv"))


class BaseChooser(BoxLayout):
    chosen_database_name = StringProperty()

    def __init__(self, **kwargs):
        BoxLayout.__init__(self, **kwargs)

        self.chosen_database_path = None
        self.chosen_database_name = "<--- choose database"
        self.database = None  # set in kv file
        Clock.schedule_once(self._load_default_database, 0)

    def _load_default_database(self, dt=0):
        try:
            self.load_database(DEFAULT_DATABASE)
        except:
            pass

    def choose_database(self, *args):
        Tk().withdraw()

        self.load_database(askopenfilename(
            filetypes=[("database", DATABASE_EXTENSION)]))

    def load_database(self, path):
        if path:
            self.database.load_database(path)
            self.chosen_database_path = path
            self.chosen_database_name = path.rsplit(os.sep, 1)[-1]
            App.get_running_app().root.ids['solver'].solve()
