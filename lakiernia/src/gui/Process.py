from kivy.factory import Factory
from kivy.uix.button import Button


class Process:
    def __init__(self, process_id, start_time, duration):
        self.process_id = process_id
        self.start_time = start_time
        self.duration = duration


class ProcessWidget(Button):
    def __init__(self, process, machines_line, **kwargs):
        Button.__init__(self, **kwargs)
        self.process = process
        self.machines_line = machines_line

    def show_details(self):
        Factory.ProcessInformation(self.process, self.machines_line).open()
