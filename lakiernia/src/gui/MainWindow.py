from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout

from src.Database import Database


class MainWindow(BoxLayout):
    database = ObjectProperty(Database())

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

