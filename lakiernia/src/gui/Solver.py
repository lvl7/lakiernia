from collections import OrderedDict
from collections import deque
from collections import namedtuple

import numpy
from kivy.uix.boxlayout import BoxLayout

from src.gui.Process import Process


class Solver(BoxLayout):
    def __init__(self, **kwargs):
        BoxLayout.__init__(self, **kwargs)

        self.database = None  # set in kv file
        self.processesViewer = None  # set in kv file

    def solve(self):
        def solve_for_processes(jobs):
            machines_line = OrderedDict()
            for machine_name in self.database.process_names:
                machines_line[machine_name] = []

            process_names = self.database.process_names
            cMax = 0
            for job_idx, job in enumerate(jobs):
                end_on_prev_machine = 0
                for inx, machine_line in enumerate(machines_line.values()):
                    try:
                        last_process = machine_line[-1]
                    except IndexError:
                        last_process = None

                    process_duration = job[1][inx]

                    last_process_end = 0
                    if last_process is not None:
                        last_process_end = last_process.duration + last_process.start_time
                    process_start_time = max(last_process_end, end_on_prev_machine)

                    new_process = Process(job[0], process_start_time, process_duration)

                    machine_line.append(new_process)
                    end_on_prev_machine = process_duration + process_start_time
                    cMax = max(cMax, end_on_prev_machine)
            return cMax, machines_line

        if self.database is not None and self.database.database is not None:
            db = self.database.database
            sorted_jobs_indexes = numpy.argsort(db[:,:].sum(1))
            sorted_jobs = db[sorted_jobs_indexes]
            sorted_jobs_names = self.database.process_ids[sorted_jobs_indexes]

            jobs = []
            for indx in range(len(sorted_jobs_names)):
                jobs.append((sorted_jobs_names[indx], sorted_jobs[indx]))

            optimal_jobs_order = deque()
            optimal_jobs_order.append(jobs[0])
            optimazed_jobs = 1

            Best_so_far = namedtuple('best_so_far', ['cMax', 'optimal_jobs_order', 'machines_line'])
            jobs_number = len(sorted_jobs)
            best = Best_so_far(float('Inf'), None, None)
            while optimazed_jobs < jobs_number:
                best = Best_so_far(float('Inf'), None, None)
                try_fit = jobs[optimazed_jobs]
                for try_position in range(optimazed_jobs + 1):
                    optimal_jobs_order.insert(try_position, try_fit)
                    cMax_try, machines_line_try = solve_for_processes(optimal_jobs_order)
                    if cMax_try < best.cMax:
                        best = Best_so_far(cMax_try, list(optimal_jobs_order), machines_line_try)
                    del optimal_jobs_order[try_position]
                optimal_jobs_order = best.optimal_jobs_order
                optimazed_jobs += 1

            self.processesViewer.visualize(best.machines_line)
