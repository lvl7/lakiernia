import collections
import random

from kivy.graphics.context_instructions import Color
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.relativelayout import RelativeLayout

from src.gui.Process import ProcessWidget

MACHINE_LAYOUT_HEIGHT = '50dp'
MACHINE_NAME_WIDTH = '100dp'

TIME_TO_WIDTH_RATIO = 20


class NamedMachineLayout(BoxLayout):
    pass


class MachineLayout(RelativeLayout):
    def __init__(self, **kw):
        super().__init__(**kw)
        self.processes = []

    def add_process(self, widget):
        RelativeLayout.add_widget(self, widget)

        if isinstance(widget, ProcessWidget):
            self.processes.append(widget)

    def get_last_process(self):
        return self.processes[-1] if self.processes else None


class VisualizeProcessesLayout(BoxLayout):
    pass


class ProcessesViewer(BoxLayout):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.processes = []
        self.database = None  # set in kv file

        self.machines_views_layouts = collections.OrderedDict()
        self.visualize_processes_layout = None

    def visualize(self, machines_line):
        def replace_old_solution():
            if self.visualize_processes_layout is not None:
                self.remove_widget(self.visualize_processes_layout)

            self.visualize_processes_layout = VisualizeProcessesLayout()
            self.add_widget(self.visualize_processes_layout)

        def make_rows():
            for machine_name, machine_line in machines_line.items():
                named_machine_layout = NamedMachineLayout(
                    height=MACHINE_LAYOUT_HEIGHT
                )

                named_machine_layout.add_widget(Label(
                    size_hint=(None, 1),
                    width=MACHINE_NAME_WIDTH,
                    text=machine_name
                ))

                machine_layout = MachineLayout(
                    size_hint=(None, 1),
                    width=int(TIME_TO_WIDTH_RATIO *
                              (machine_line[-1].start_time + machine_line[-1].duration))
                )

                named_machine_layout.add_widget(machine_layout)
                self.visualize_processes_layout.add_widget(named_machine_layout, )
                self.machines_views_layouts[machine_name] = machine_layout

        def show_processes():
            hue = random.random()
            for idx in range(len(machines_line[list(machines_line.keys())[0]])):
                hue += 0.2 + random.random()*0.2
                hue %= 1
                color = tuple(Color(hue,0.8, 0.5, mode='hsv').rgba)
                for machine_name, machine_line in machines_line.items():
                    process = machine_line[idx]
                    process_width =int(process.duration * TIME_TO_WIDTH_RATIO)
                    self.machines_views_layouts[machine_name].add_process(ProcessWidget(
                        process,
                        machines_line,
                        size_hint=(None, 1),
                        width=process_width,
                        pos=(int(process.start_time * TIME_TO_WIDTH_RATIO), 0),
                        text=process.process_id,
                        text_size=(process_width, 50),
                        halign='center',
                        valign='center',
                        background_normal='(1,1,1,1)',
                        background_color=color
                    ))

        replace_old_solution()
        make_rows()
        show_processes()
