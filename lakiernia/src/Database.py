import numpy

DATABASE_SEPARATOR = "\t"


class Database:
    def __init__(self):
        self.process_names = []
        self.database = None
        self.process_ids = None

    def load_database(self, path):
        def id_convert(txt):
            # return txt.decode("utf-8")
            return txt

        with open(path) as file:
            self.process_names = file.readline()[:-1].split(DATABASE_SEPARATOR)[1:]

        self.process_ids = numpy.loadtxt(path, delimiter=DATABASE_SEPARATOR, dtype=str,
                                         skiprows=1,
                                         usecols=0)

        columns = range(1, len(self.process_names) + 1)
        self.database = numpy.loadtxt(path, delimiter=DATABASE_SEPARATOR,
                                      skiprows=1,
                                      usecols=columns)
